package org.tvo.libs;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Gaurang Shah.
 */
public class ReadConfig {

    private Properties prop;

    public ReadConfig(String fileName){
        prop = new Properties();
        try {
            FileInputStream input = new FileInputStream(System.getProperty("user.dir")+"/src/test/java/org/tvo/config/"+fileName);
            prop.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Read the value from the property File.
     * @param key Key for which you want to get value
     * @return Value from the file.
     */
    public String getValue(String key){
        return prop.getProperty(key);
    }
}
