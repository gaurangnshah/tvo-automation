package org.tvo.libs.ui;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.Augmenter;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.internal.IResultListener2;

import java.io.File;
import java.io.IOException;

/**
 * Created by Gaurang Shah.
 * Implements IResultLister2 class of TestNG to take screenshot whenever test case fails.
 */

public class ResultListener extends WebDriverManager implements IResultListener2 {

    public void onTestStart(ITestResult result) {
    }

    public void onTestSuccess(ITestResult result) {

    }

    public void onTestFailure(ITestResult result) {
        String fileName = result.getTestContext().getName()+"_"+result.getName();
        try {
            takeScreenshot(fileName);
        }catch (Exception e){
            System.out.println("Failed to take the screenshot "+fileName);
            e.printStackTrace();
        }


    }

    public void onTestSkipped(ITestResult result) {

    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    public void onStart(ITestContext context) {

    }

    public void onFinish(ITestContext context) {

    }

    public void onConfigurationSuccess(ITestResult itr) {

    }

    public void onConfigurationFailure(ITestResult itr) {

    }

    public void onConfigurationSkip(ITestResult itr) {

    }

    public void beforeConfiguration(ITestResult tr) {

    }

    private void takeScreenshot(String filename){
        driver = new Augmenter().augment(driver);
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String filePath =  System.getProperty("user.dir")+"/target/surefire-reports/html/screenshots/"+filename+".png";
        try {
            FileUtils.copyFile(scrFile, new File(filePath));
        } catch (IOException ioe) {
            ioe.printStackTrace();
            throw new RuntimeException(ioe);
        }

        Reporter.log("<a href=screenshots/"+filename+".png> <img src='screenshots/"+filename+".png' height='150' width='250'/>  </a>");
    }

}
