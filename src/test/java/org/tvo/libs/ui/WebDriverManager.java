package org.tvo.libs.ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.tvo.libs.ReadConfig;

/**
 * Created by Gaurang Shah.
 * Abstract class which manages the webdriver object. All the test class must extend this class.
 */
public abstract class WebDriverManager {

    protected static WebDriver driver;

    @BeforeMethod
    @Parameters(value={"browserName"})
    public void setUp(@Optional("chrome") String browser) {
        String baseURL = new ReadConfig("config.properties").getValue("baseURL");
        createDriver(browser);
        driver.get(baseURL);

    }

    /**
     *  Based on the parameter passed creates the object of browser and other necessary configuration
     * @param browser browser you want to run your tests on
     */
    private void createDriver(String browser){
        if (browser.equalsIgnoreCase("Firefox")){
            createFirefoxDriver();
        }
        if (browser.equalsIgnoreCase("Chrome")){
            createChromeDriver();
        }
    }

    /**
     * Creates the object of chromeDriver and other necessary configuration
     */
    private void createChromeDriver() {

        ReadConfig readConfig = new ReadConfig("config.properties");
        System.setProperty("webdriver.chrome.driver", readConfig.getValue("chromedriver.path"));
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    /**
     * Creates the object of firefoxDriver and other necessary configuration
     */
    private void createFirefoxDriver() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();

    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(){
        driver.close();
        driver.quit();
    }



}
