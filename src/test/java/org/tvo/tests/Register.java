package org.tvo.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.tvo.libs.ui.WebDriverManager;
import org.tvo.pages.HomePage;
import org.tvo.pages.RegisterPage;

/**
 * Created by Gaurang Shah.
 */
public class Register extends WebDriverManager {

    @BeforeMethod
    public void gotoRegisterPage(){
        new HomePage(driver).gotoRegisterPage();
    }

    @Test
    public void shouldBeAbleToRegisterNewUser(){

        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.fillForm("Gaurang", "Shah", "Administrator", "Algoma DSB",
                "Anna McCrea Public School", "testuser11@adsb.ca", "testuser11@adsb.ca",
                "abc12345678","abc12345678", "Another Educator",true)
            .submit();
        registerPage.successMessageShouldBe("Thank you for registering!");

    }

    @Test
    public void minimumPasswordLengthShouldBe8Char(){
        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.fillForm("Gaurang", "Shah", "Administrator", "Algoma DSB",
                "Anna McCrea Public School", "testuser11@adsb.ca", "testuser11@adsb.ca",
                "abc1234","abc1234", "Another Educator",true)
                .submit();

        registerPage.alertMessageShouldBe("Your password must be 8-32 characters long");
    }

    @Test
    public void passwordAndConfirmPasswordShouldMatch(){
        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.fillForm("Gaurang", "Shah", "Administrator", "Algoma DSB",
                "Anna McCrea Public School", "testuser11@adsb.ca", "testuser11@adsb.ca",
                "abc12345678","abc12345679", "Another Educator",true)
                .submit();

        registerPage.alertMessageShouldBe("Your passwords do not match");
    }

    @Test
    public void emailAndConfirmEmailShouldMatch(){
        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.fillForm("Gaurang", "Shah", "Administrator", "Algoma DSB",
                "Anna McCrea Public School", "testuser1@adsb.ca", "testuser11@adsb.com",
                "abc12345678","abc12345678", "Another Educator",true)
                .submit();

        registerPage.alertMessageShouldBe("Please make sure your email matches");
    }

    @Test
    public void emailAddressShouldMatchAsPerSchoolBoard(){
        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.fillForm("Gaurang", "Shah", "Coach", "Algoma DSB",
                "Anna McCrea Public School", "testuser11@test.com", "testuser11@test.com",
                "abc12345678","abc12345678", "Another Educator",true)
                .submit();

        String errMsg="The email you want to use does not match the email of your school board. " +
                "The domain of your email address should be adsb.on.ca, email.adsb.on.ca,adsb.ca";
        registerPage.alertMessageShouldBe(errMsg);
    }

    @Test
    public void verifyErrorMessagesWhenFieldsAreEmpty(){

        String[] errorMessages = new String[] {
                "Please enter your first name",
                "Please enter your last name",
                "Please select a role",
                "Please select a school board",
                "Please add your school",
                "Please enter a valid email address",
                "Your password must be 8-32 characters long",
                "Please tell us where you heard about mPower",
                "You must check agree to continue"
        };

        RegisterPage registerPage = new RegisterPage(driver);

        registerPage.fillForm("", "", "", "", "", "", "", "", "", "", false).submit();
        registerPage.fieldValidationMessageShouldBe(errorMessages);
    }

    @Test
    public void firstNameAndLastNameLengthShouldBeLessThan42Char(){
        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.enterFirstName("12345678901234567890123456789012345678901")
                .submit()
                .alertMessageShouldBe("The first name specified is of an invalid length")
                .enterFirstName("Gaurang")
                .enterLastName("12345678901234567890123456789012345678901")
                .submit()
                .alertMessageShouldBe("The last name specified is of an invalid length");
    }
}
