package org.tvo.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.*;

/**
 * Created by Gaurang Shah.
 */
public class RegisterPage extends Page{

    private By firstNameEditbox = By.id("firstName");
    private By lastNameEditbox = By.id("lastName");
    private By roleDropdown = By.id("educatorRoleId");
    private By boardDropdown = By.id("boardId");
    private By schoolDropdown = By.id("schoolId");
    private By emailEditbox = By.id("email");
    private By confirmEmailEditbox = By.id("userEmailConfirm");
    private By passwordEditbox = By.id("userPassword");
    private By confirmPasswordEditbox = By.id("userPasswordConfirm");
    private By sourceDropdown = By.id("source");
    private By tncCheckbox = By.id("agreement");
    private By registerButton = By.id("registerSubmit");
    private By alertMessage = By.xpath("//div[not(contains(@style,'display:none'))]//*[@class='alert alert-danger' or @class='alert alert-danger ng-isolate-scope']");
    private By successMessage = By.xpath("//h2");

    public RegisterPage(WebDriver driver) {
        super(driver);
        waitUntilElementIsPresent(firstNameEditbox);
    }

    public RegisterPage fillForm(String firstName, String lastName, String role, String board, String school, String email,
                                String confirmEmail, String password, String confirmPassword, String source,
                                boolean tnc) {

        enterFirstName(firstName)
                .enterLastName(lastName)
                .selectRole(role)
                .selectBoard(board)
                .selectSchool(school)
                .enterEmail(email)
                .enterConfirmEmail(confirmEmail)
                .enterPassword(password)
                .enterConfirmPassword(confirmPassword)
                .selectTermsAndConditions(tnc)
                .selectSource(source);

        return this;
    }

    public RegisterPage enterFirstName(String firstName){

        findElementWithWait(firstNameEditbox).clear();
        sendCharts(firstNameEditbox, firstName);
        return this;
    }

    public RegisterPage enterLastName(String lastName){
        findElement(lastNameEditbox).clear();
        sendCharts(lastNameEditbox, lastName);
        return this;
    }

    public RegisterPage selectRole(String role){
        if (role.length() > 0)
            findDropdown(roleDropdown).selectByVisibleText(role);
        return this;
    }

    public RegisterPage selectBoard(String board){
        if(board.length() > 0)
            findDropdown(boardDropdown).selectByVisibleText(board);
        return this;

    }

    public RegisterPage selectSchool(String school){
        if (school.length() > 0 )
            findDropdown(schoolDropdown).selectByVisibleText(school);
        return this;

    }

    public RegisterPage enterEmail(String email){
        findElement(emailEditbox).sendKeys(email);
        return this;
    }

    public RegisterPage enterConfirmEmail(String email){
        findElement(confirmEmailEditbox).sendKeys(email);
        return this;
    }

    public RegisterPage enterPassword(String password){
        findElement(passwordEditbox).sendKeys(password);
        return this;
    }

    public RegisterPage enterConfirmPassword(String password){
        findElement(confirmPasswordEditbox).sendKeys(password);
        return this;
    }


    public RegisterPage selectSource(String source){
        if(source.length() > 0)
            new Select(findElement(sourceDropdown)).selectByVisibleText(source);
        return this;
    }

    public RegisterPage selectTermsAndConditions(boolean tnc){
        if(tnc){
            scrollTo(tncCheckbox);
            findElement(tncCheckbox).click();
        }
        return this;
    }

    public RegisterPage submit(){
        scrollTo(registerButton);
        findElement(registerButton).click();
        return this;
    }

    public RegisterPage alertMessageShouldBe(String message){
        waitUntilElementIsPresent(alertMessage);
        Assert.assertEquals(findElement(alertMessage).getText(), message);
        return this;
    }

    public void successMessageShouldBe(String message){
        waitUntilElementIsPresent(successMessage);
        Assert.assertEquals(findElement(successMessage).getText(), message);

    }


    /**
     * Verifies Field validation messages, this are different from error message appears on the top, after you submit form.
     * For verifies that message use alertMessageShouldBe method.
     * @param errorMessages error Messages
     */
    public void fieldValidationMessageShouldBe(String[] errorMessages){

        Iterator<WebElement> iterator = findElements(alertMessage).iterator();
        List<String> actualErrorMessages = new ArrayList<String>();
        List<String> expectedErrorMessages = new ArrayList<String>(Arrays.asList(errorMessages));

        while (iterator.hasNext()){
            actualErrorMessages.add(iterator.next().getText());
        }

        boolean errorMsgMatched = true;
        System.out.println(actualErrorMessages.equals(expectedErrorMessages));

        String message="";
        for (String msg: errorMessages) {
            message = msg;
            if(!actualErrorMessages.contains(msg) ){
                errorMsgMatched = false;

            }
        }

        Assert.assertTrue(errorMsgMatched, message+" Not Found in "+actualErrorMessages);
    }


}
