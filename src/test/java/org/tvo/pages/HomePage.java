package org.tvo.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Gaurang Shah.
 */
public class HomePage extends Page {

    private By registerButton  = By.id("loginPageRegister1");

    public HomePage(WebDriver driver) {
        super(driver);
        waitUntilElementIsPresent(registerButton);
    }

    public void gotoRegisterPage(){
        findElement(registerButton).click();
    }
}
