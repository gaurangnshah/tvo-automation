package org.tvo.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Gaurang Shah.
 *
 * All the Page Class must extend this class
 */
abstract class Page {

    private WebDriver driver;
    private WebDriverWait wait;


    Page(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    /**
     *  Returns the Webelement object
     * @param identifier By object
     * @return WebElement Object
     */
    WebElement findElement(By identifier){
        return driver.findElement(identifier);
    }


    /**
     *  Returns the Webelement object
     * @param identifier By object
     * @return WebElement Object
     */
    WebElement findElementWithWait(By identifier){
        WebElement element = waitUntilElementIsPresent(identifier);
        waitUntilElementIsVisible(element);
        return element;
    }


    /**
     * Returns the multiple elements matching by identifier
     * @param identifier By Object
     * @return WebElements
     */
    List<WebElement> findElements(By identifier){
        return driver.findElements(identifier);
    }


    //Wait Methods

    /**
     * Waits until Element Present, By default timeout is 30 seconds
     * @param identifier By Objects
     */
    WebElement waitUntilElementIsPresent(By identifier) {
        return waitUntilElementIsPresent(identifier, 30);
    }

    /**
     * Wait until Element presents or timed out
     * @param identifier By Object
     * @param timeout time until which you want to wait.
     */
    WebElement waitUntilElementIsPresent(By identifier, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        return wait.until(ExpectedConditions.presenceOfElementLocated(identifier));
    }

    /**
     * Waits until Element is visible, Timeout is 30 seconds
     * @param element WebElement Object
     */
    void waitUntilElementIsVisible(WebElement element){
        waitUntilElementIsVisible(element, 30);
    }

    /**
     * Wait Until Element is visible or timed out
     * @param element WebElement Object
     * @param timeout time until you want to wait.
     */
    void waitUntilElementIsVisible(WebElement element, int timeout){
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.visibilityOf(element));
    }


    /**
     * Waits until dropdown contains more than 2 options.
     * @param dropdown Select Object
     */
    public void waitUntilDropdownContainsOptions(final Select dropdown){
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver){
                return dropdown.getOptions().size() > 2;
            }
        });
    }

    /**
     * Using javascript scrolls to the given element
     * @param identifier By Object
     */
    void scrollTo(By identifier){

        //ActionClass MoveToElement is not working from chrome Browser with current configuration.
        WebElement element = findElement(identifier);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

    }

    /**
     * Creates the select object and returns back
     * @param identifier By Object
     * @return Select Object
     */

    Select findDropdown(By identifier){
        Select dropdown = new Select(findElementWithWait(identifier));
        waitUntilDropdownContainsOptions(dropdown);
        return dropdown;

    }


    /**
     * Sends charcter of string one by one. Fix for FirstName and LastName field
     * @param identifier By Object
     * @param str String to be send to editbox
     */
    void sendCharts(By identifier, String str){
        WebElement elem = findElementWithWait(identifier);
        List<String> chars = new ArrayList<String>(Arrays.asList(str.split("")));
        for (String chr: chars) {

            elem.sendKeys(chr);
        }
    }
}
